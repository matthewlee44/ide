adduser matt
usermod -aG sudo matt
rsync --archive --chown=matt:matt ~/.ssh /home/matt
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
cd /home/matt/
git clone https://matthewlee44@bitbucket.org/matthewlee44/ide.git
cd ide
docker-compose -f nginx-proxy-compose.yaml up -d
sudo apt install apache2-utils
sudo mkdir -p /etc/nginx/htpasswd
sudo touch /etc/nginx/htpasswd/ide.marvelcapital.ml
sudo htpasswd /etc/nginx/htpasswd/ide.marvelcapital.ml matt
docker-compose -f eclipse-theia-compose.yaml up -d